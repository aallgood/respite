use serenity::{
    builder::CreateApplicationCommand,
    model::prelude::{
        command::CommandOptionType,
        interaction::application_command::{
            CommandDataOption,
            CommandDataOptionValue,
        }
    }
};

pub fn run(options: &[CommandDataOption]) -> String {
    let user = options
        .get(0)
        .expect("Expected user input")
        .resolved
        .as_ref()
        .expect("Requires a user input");

    if let CommandDataOptionValue::User(user, _) = user {
        dbg!(user);
        "todo".to_string()
    }
    else {
        return "Some error has occurred within commands/ban.rs".to_string();
    }
}

pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
    command
        .name("ban")
        .description("Bans the identified user")
        .create_option(|option| {
            option
                .name("user")
                .description("The user to ban")
                .kind(CommandOptionType::User)
                .required(true)
        }
        )
        .create_option(|option | {
            option
                .name("reason")
                .description("The audit log reason to ban")
                .kind(CommandOptionType::String)
                .required(false)
            }
        )
        .create_option(|option | {
            option
                .name("delete_messages")
                .description("Should the user's messages be deleted?")
                .kind(CommandOptionType::Boolean)
                .required(false)
            }
        )
        .create_option(|option | {
            option
                .name("allow_appeal")
                .description("Allow the user to appeal?")
                .kind(CommandOptionType::Boolean)
            }
        )
        .create_option(|option | {
            option
                .name("duration")
                .description("When Respite will lift the ban")
                .kind(CommandOptionType::String)
                .required(false)
            }
        )
}
