// Add more win-lose-draw messages. Probably an array (or Vec<>) of Strings

use rand::{
    distributions::{
        Distribution,
        Uniform
    },
    thread_rng
};
use twilight_http::Client;
use twilight_model::{
    application::interaction::{
        application_command::CommandOptionValue,
        Interaction
    },
    http::interaction::{
        InteractionResponse,
        InteractionResponseType
    }
};
use twilight_model::application::interaction::application_command::CommandData;
use twilight_util::builder::{
    embed::EmbedBuilder,
    InteractionResponseDataBuilder
};

pub async fn run(interaction: Interaction, client: &Client, data: CommandData) -> anyhow::Result<()> {
    let client = client.interaction(interaction.application_id);

    let parsed = data
        .options
        .iter()
        .next()
        .unwrap()
        .value
        .clone();

    let computer_choice: i64 = Uniform::from(0..=2)
        .sample(&mut thread_rng());

    let mut title: String = "".to_string();
    let mut desc: String = "".to_string();
    if let CommandOptionValue::Integer(user_choice) = parsed {
        if user_choice == computer_choice { // user & computer same choice
            title = "Draw!".to_string();
            desc = "You and the computer chose the same! Try again!".to_string();
        }
        else if ( // user wins
            user_choice == 0 && computer_choice == 2 // rock - scissors
        ) || (
            user_choice == 1 && computer_choice == 0
        ) || ( user_choice == 2 && computer_choice == 1
        ) {
            title = "You Win!".to_string();
            desc = "You have reigned victorious over the machine! Try again to assert your dominance!".to_string();
        }
        else { // computer wins
            title = "You Lost!".to_string();
            desc = "You have lost the battle, but the war still rages! Try again to regain your house's honor!".to_string();
        }
    }

    let e = EmbedBuilder::new()
        .title(title)
        .description(desc)
        //.image(&user.avatar) ///No reason to continue looking into this until I can turn the bytes into something useful
        .validate()?
        .build();

    let res = InteractionResponseDataBuilder::new();
    let res = res.embeds([e]);

    let response = InteractionResponse {
        kind: InteractionResponseType::ChannelMessageWithSource,
        data: Some(res.build()),
    };
    client
        .create_response(interaction.id, &interaction.token, &response)
        .await?;
    Ok(())

}
