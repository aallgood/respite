use serenity::{
    builder::CreateApplicationCommand,
    model::prelude::{
        command::CommandOptionType,
        interaction::application_command::{
            CommandDataOption,
            CommandDataOptionValue,
        }
    }
};
use rand::Rng;

pub fn run(options: &[CommandDataOption]) -> String {
    let amount = options
        .get(0)
        .expect("Expected integer input")
        .resolved
        .as_ref()
        .expect("Requires an integer input");
    let mut values: [u16; 6] = [0, 0, 0, 0, 0, 0];
    let mut rng = rand::thread_rng();


    if let CommandDataOptionValue::Integer(amount) = amount {
        for _ in 0..*amount {
            values[rng.gen_range(0..=5)] += 1
        }

        let dice: [String; 6] = [
            "<:dice_one:665298149274943550> ".to_string(),
            "<:dice_two:711992588709920779> ".to_string(),
            "<:dice_three:711992763209744415> ".to_string(),
            "<:dice_four:711992276724744253> ".to_string(),
            "<:dice_five:711992019349799005> ".to_string(),
            "<:dice_six:665298304732495892> ".to_string(),
        ];

        let res: String = dice.iter()
            .zip(values)
            .map(|(dice, value)| format!("{dice} {value}"))
            .collect::<Vec<_>>()
            .join("\n");

        /*let mut res: String = "<:dice_one:665298149274943550> ".to_owned();
        let acc0: &str = &acc[0].to_string();
        res.push_str(acc0);
        res.push_str("\n");
        let two: String = "<:dice_two:711992588709920779> ".to_string();
        res.push_str(&two);
        let acc1: &str = &acc[1].to_string();
        res.push_str(acc1);
        res.push_str("\n");
        let three: String = "<:dice_three:711992763209744415> ".to_string();
        res.push_str(&three);
        let acc2: &str = &acc[2].to_string();
        res.push_str(acc2);
        res.push_str("\n");
        let four: String = "<:dice_four:711992276724744253> ".to_string();
        res.push_str(&four);
        let acc3: &str = &acc[3].to_string();
        res.push_str(acc3);
        res.push_str("\n");
        let five: String = "<:dice_five:711992019349799005> ".to_string();
        res.push_str(&five);
        let acc4: &str = &acc[4].to_string();
        res.push_str(acc4);
        res.push_str("\n");
        let six: String = "<:dice_six:665298304732495892> ".to_string();
        res.push_str(&six);
        let acc5: &str = &acc[5].to_string();
        res.push_str(acc5);
        res.push_str("\n");*/
        res
    }
    else {
        return "Some error has occurred within commands/roll.rs".to_string();
    }
}

pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
    command
        .name("dice")
        .description("Rolls a six-sided die up to 65,535 times")
        .create_option(|option| {
            option
                .name("amount")
                .description("An integer between 1 and 65,535")
                .kind(CommandOptionType::Integer)
                .min_int_value(1)
                .max_int_value(65535)
                .required(true)
        })
}
