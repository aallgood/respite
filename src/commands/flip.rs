use serenity::{
    builder::CreateApplicationCommand,
    model::prelude::{
        command::CommandOptionType,
        interaction::application_command::{
            CommandDataOption,
            CommandDataOptionValue,
        }
    }
};
use rand::Rng;

pub fn run(options: &[CommandDataOption]) -> String {
    let amount = options
        .get(0)
        .expect("Expected integer input")
        .resolved
        .as_ref()
        .expect("Requires an integer input");
    let mut acc: [u16; 2] = [0, 0];
    let mut rng = rand::thread_rng();

    if let CommandDataOptionValue::Integer(amount) = amount {
        for _ in 0..*amount {
            let i = rng.gen_range(0..=1);
            acc[i] += 1;
        }
        let mut res: String = "Heads: ".to_owned();
        let acc0: &str = &acc[0].to_string();
        res.push_str(acc0);
        res.push_str("\n");
        let two: String = "Tails: ".to_string();
        res.push_str(&two);
        let acc1: &str = &acc[1].to_string();
        res.push_str(acc1);
        res
    }
    else {
        return "Some error has occurred within commands/flip.rs".to_string();
    }
}

pub fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
    command
        .name("flip")
        .description("Flips a coin up to 65,535 times")
        .create_option(|option| {
            option
                .name("amount")
                .description("An integer between 1 and 65,535")
                .kind(CommandOptionType::Integer)
                .min_int_value(1)
                .max_int_value(65535)
                .required(true)
        })
}
