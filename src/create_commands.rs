use anyhow::Result;
use twilight_http::client::InteractionClient;
use twilight_model::{
    application::command::CommandType,
    guild::Permissions
};
use twilight_model::application::command::{CommandOption, CommandOptionChoice, CommandOptionChoiceValue, CommandOptionType};
use twilight_util::builder::command::{BooleanBuilder, CommandBuilder, IntegerBuilder, StringBuilder, UserBuilder};

pub async fn create(interaction_client: InteractionClient<'_>) -> Result<()> {
    let ban = CommandBuilder::new("ban", "ban a user, add an optional reason for the audit log", CommandType::ChatInput)
        .nsfw(false)
        .default_member_permissions(Permissions::BAN_MEMBERS)
        .option(UserBuilder::new("user", "the user to ban")
            .required(true)
            .build())
        .option(BooleanBuilder::new("delete_recent", "deletes messages from the past seven days")
            .required(true)
            .build())
        .option(StringBuilder::new("reason", "the reason entered into the audit log")
            .required(false)
            .build())
        .validate()?
        .build();

    let chuck = CommandBuilder::new("chuck", "chuck norris facts", CommandType::ChatInput)
        .nsfw(false)
        .default_member_permissions(Permissions::USE_SLASH_COMMANDS)
        .validate()?
        .build();

    let flip = CommandBuilder::new("flip", "flip a coin", CommandType::ChatInput)
        .nsfw(false)
        .default_member_permissions(Permissions::USE_SLASH_COMMANDS)
        .option(IntegerBuilder::new("amount", "how many coins to flip")
            .required(true)
            .min_value(1)
            .max_value(1024)
            .build())
        .validate()?
        .build();

    let ping = CommandBuilder::new("ping", "basic ping command", CommandType::ChatInput)
        .nsfw(false)
        .default_member_permissions(Permissions::USE_SLASH_COMMANDS)
        .validate()?
        .build();

    let roll = CommandBuilder::new("roll", "roll a die", CommandType::ChatInput)
        .nsfw(false)
        .default_member_permissions(Permissions::USE_SLASH_COMMANDS)
        .option(IntegerBuilder::new("amount", "how many dice to roll")
            .required(true)
            .min_value(1)
            .max_value(1024)
            .build())
        .validate()?
        .build();

    let rps = CommandBuilder::new("rps", "play rock paper scissors against the bot", CommandType::ChatInput)
        .nsfw(false)
        .default_member_permissions(Permissions::USE_SLASH_COMMANDS)
        .option(
            CommandOption {
                autocomplete: None,
                channel_types: None,
                choices: None,
                description: "rock paper scissors".to_string(),
                description_localizations: None,
                kind: CommandOptionType::SubCommand,
                max_length: None,
                max_value: None,
                min_length: None,
                min_value: None,
                name: "choice".to_string(),
                name_localizations: None,
                options: Option::from(
                    CommandOption
                ),
                required: None,
            }
        )
        .validate()?
        .build();

    if let Err(error) = interaction_client.set_global_commands(&[ban, chuck, flip, ping, roll, rps]).await {
        tracing::error!(?error, "failed to register commands");
    }
    Ok(())
}