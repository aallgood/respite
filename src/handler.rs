use std::sync::Arc;
use std::string::String;
use std::time::{
    SystemTime,
    UNIX_EPOCH
};
use anyhow::Result;
use serde::{
    Deserialize,
    Serialize
};
use sqlx::{
    Executor,
    FromRow,
    Pool,
    Postgres};
use sqlx::postgres::PgPoolOptions;
use twilight_cache_inmemory::InMemoryCache;
use twilight_http::Client;
use twilight_model::{
    id::{
        Id,
        marker::ChannelMarker
    },
    gateway::{
        CloseFrame,
        payload::incoming::*
    },
    guild::auto_moderation::AutoModerationEventType::MessageSend,
    util::Timestamp
};
use twilight_util::builder::embed::{
    EmbedBuilder,
    EmbedFooterBuilder
};
use twilight_util::snowflake::Snowflake;

pub async fn auto_moderation_action_execution(
    incoming: AutoModerationActionExecution,
    client: &Client,
    pool: &Pool<Postgres>
) -> Result<()> {
    /*dbg!(&incoming);
    let amae = sqlx::query("SELECT amae_notify FROM guilds WHERE guild_id = $1")
        .bind(incoming.guild_id.id() as i64)
        .execute(pool)
        .await?;
    dbg!(amae);*/
    Ok(())
}

pub async fn auto_moderation_rule_create(
    incoming: Box<AutoModerationRuleCreate>,
    client: &Client
) -> Result<()> {
    dbg!(&incoming);
    Ok(())
}

pub async fn auto_moderation_rule_delete(
    incoming: Box<AutoModerationRuleDelete>,
    client: &Client
) -> Result<()> {
    dbg!(&incoming);
    Ok(())
}

pub async fn auto_moderation_rule_update(
    incoming: Box<AutoModerationRuleUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(&incoming);
    Ok(())
}

#[derive(FromRow, Debug, Serialize, Deserialize)]
struct MyCustomResponse {
    dnc: String,
    ban: bool,
    bacni: String
}

pub async fn ban_add(
    ban: BanAdd,
    client: &Client,
    pool: &Pool<Postgres>
) -> Result<()> {
    dbg!(&ban);

    let guild_id: u64 = ban.guild_id.id();
    dbg!(guild_id);
    /*let ban_add_channel_notification_id = sqlx::query_as::<Postgres, MyCustomResponse>("SELECT default_notify_channel, ban_add_notify, ban_add_notification_channel_id FROM guilds WHERE guild_id = $1")
        .bind(guild_id.to_string())
        .fetch_one(pool)
        .await?;
    dbg!(ban_add_channel_notification_id.dnc);*/

    let user = ban.user;
    let e = EmbedBuilder::new()
        .color(0xff0000)
        .title("User Banned")
        .description(format!("uid: {0} ,  is_bot: {1} ,  is_verified: {2:?}", user.id, user.bot, user.verified))
        //.timestamp()
        .validate()?
        .build();
    client
        .create_message(Id::new(955789263151185943))
        .embeds(&[e])?
        .await?;
    //dbg!(&[e]);
    Ok(())
}

pub async fn ban_remove(
    ban: BanRemove,
    client: &Client
) -> Result<()> {
    dbg!(&ban);
    Ok(())
}

pub async fn channel_create(
    channel: Box<ChannelCreate>,
    client: &Client
) -> Result<()> {
    dbg!(&channel);
    Ok(())
}

pub async fn channel_delete(
    channel: Box<ChannelDelete>,
    client: &Client
) -> Result<()> {
    dbg!(&channel);
    Ok(())
}

pub async fn channel_pins_update(
    channel: ChannelPinsUpdate,
    client: &Client
) -> Result<()> {
    dbg!(&channel);
    Ok(())
}

pub async fn channel_update(
    channel: Box<ChannelUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(&channel);
    Ok(())
}

pub async fn command_permissions_update(
    command: CommandPermissionsUpdate,
    client: &Client
) -> Result<()> {
    dbg!(&command);
    Ok(())
}

pub async fn gateway_close(
    gateway: Option<CloseFrame<'_>>,
    client: &Client
) -> Result<()> {
    dbg!(&gateway);
    Ok(())
}

pub async fn gateway_heartbeat(
    gateway: u64,
    client: &Client
) -> Result<()> {
    dbg!(&gateway);
    Ok(())
}

pub async fn gateway_hello(
    gateway: Hello,
    client: &Client
) -> Result<()> {
    dbg!(&gateway);
    Ok(())
}

pub async fn gateway_invalidate_session(
    session: bool,
    client: &Client
) -> Result<()> {
    dbg!(&session);
    Ok(())
}

pub async fn guild_auditlog_entry_create(
    entry: Box<GuildAuditLogEntryCreate>,
    client: &Client
) -> Result<()> {
    dbg!(&entry);
    Ok(())
}

pub async fn guild_create(
    guild: Box<GuildCreate>,
    client: &Client
) -> Result<()> {
    dbg!(&guild);
    Ok(())
}

pub async fn guild_delete(
    guild: GuildDelete,
    client: &Client
) -> Result<()> {
    dbg!(&guild);
    Ok(())
}

pub async fn guild_emojis_update(
    guild: GuildEmojisUpdate,
    client: &Client
) -> Result<()> {
    dbg!(&guild);
    Ok(())
}

pub async fn guild_integrations_update(
    guild: GuildIntegrationsUpdate,
    client: &Client
) -> Result<()> {
    dbg!(&guild);
    Ok(())
}

pub async fn guild_scheduled_event_create(
    event: Box<GuildScheduledEventCreate>,
    client: &Client
) -> Result<()> {
    dbg!(&event);
    Ok(())
}

pub async fn guild_scheduled_event_delete(
    event: Box<GuildScheduledEventDelete>,
    client: &Client
) -> Result<()> {
    dbg!(&event);
    Ok(())
}

pub async fn guild_scheduled_event_update(
    event: Box<GuildScheduledEventUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(&event);
    Ok(())
}

pub async fn guild_scheduled_event_user_add(
    event: GuildScheduledEventUserAdd,
    client: &Client
) -> Result<()> {
    dbg!(&event);
    Ok(())
}

pub async fn guild_scheduled_event_user_remove(
    event: GuildScheduledEventUserRemove,
    client: &Client
) -> Result<()> {
    dbg!(&event);
    Ok(())
}

pub async fn guild_sticker_update(
    guild: GuildStickersUpdate,
    client: &Client
) -> Result<()> {
    dbg!(&guild);
    Ok(())
}

pub async fn guild_update(
    guild: Box<GuildUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(&guild);
    Ok(())
}

pub async fn integration_create(
    integration: Box<IntegrationCreate>,
    client: &Client
) -> Result<()> {
    dbg!(&integration);
    Ok(())
}

pub async fn integration_delete(
    integration: IntegrationDelete,
    client: &Client
) -> Result<()> {
    dbg!(&integration);
    Ok(())
}

pub async fn integration_update(
    integration: Box<IntegrationUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(&integration);
    Ok(())
}

pub async fn invite_create(
    invite: Box<InviteCreate>,
    client: &Client
) -> Result<()> {
    dbg!(invite);
    Ok(())
}

pub async fn invite_delete(
    invite: InviteDelete,
    client: &Client
) -> Result<()> {
    dbg!(&invite);
    Ok(())
}

pub async fn member_add(
    member: Box<MemberAdd>,
    client: &Client
) -> Result<()> {
    dbg!(&member);
    Ok(())
}

pub async fn member_remove(
    member: MemberRemove,
    client: &Client
) -> Result<()> {
    dbg!(&member);
    Ok(())
}

pub async fn member_update(
    member: Box<MemberUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(&member);
    Ok(())
}

pub async fn member_chunk(
    member: MemberChunk,
    client: &Client
) -> Result<()> {
    dbg!(&member);
    Ok(())
}

pub async fn message_create(
    message: Box<MessageCreate>,
    client: &Client
) -> Result<()> {
    dbg!(message);
    Ok(())
}

pub async fn message_delete(
    message: MessageDelete,
    cache: &Arc<InMemoryCache>,
    client: &Client,
    pool: &Pool<Postgres>
) -> Result<()> {
    dbg!(&message);
    //let channel_id = message.clone().channel_id.to_string();
    /*let m = cache.message(message.id);
    if let InMemoryCache::message(message_content) = m.unwrap().content() {
        let e = EmbedBuilder::new()
            .title("Message Deleted")
            .description(format!("{message_content}"))
            .validate()?
            .build();
        client
            .create_message(message.channel_id)
            .embeds(&[e])?
            .await?;
    }*/
    Ok(())
}

pub async fn message_delete_bulk(
    messages: MessageDeleteBulk,
    client: &Client
) -> Result<()> {
    dbg!(&messages);
    Ok(())
}

pub async fn message_update(
    message: Box<MessageUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(message);
    Ok(())
}

pub async fn presence_update(
    presence: Box<PresenceUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(presence);
    Ok(())
}

pub async fn reaction_add(
    reaction: Box<ReactionAdd>,
    client: &Client
) -> Result<()> {
    dbg!(reaction);
    Ok(())
}

pub async fn reaction_remove(
    reaction: Box<ReactionRemove>,
    client: &Client
) -> Result<()> {
    dbg!(reaction);
    Ok(())
}

pub async fn reaction_remove_all(
    reaction: ReactionRemoveAll,
    client: &Client
) -> Result<()> {
    dbg!(reaction);
    Ok(())
}

pub async fn reaction_remove_emoji(
    reaction: ReactionRemoveEmoji,
    client: &Client
) -> Result<()> {
    dbg!(reaction);
    Ok(())
}

pub async fn ready(
    ready: &Ready,
    client: &Client,
    pool: &Pool<Postgres>
) -> Result<()> {
    dbg!(&ready);
    let guilds = client.current_user_guilds().await?.model().await?;
    for guild in guilds {
        let res = sqlx::query::<Postgres>("INSERT INTO guilds VALUES ($1)")
            .bind(guild.id.id() as i64)
            .execute(pool)
            .await?;
        dbg!(res);
    }
    Ok(())
}

pub async fn resumed(
    client: &Client
) -> Result<()> {
    dbg!(&client);
    Ok(())
}

pub async fn role_create(
    role: RoleCreate,
    client: &Client,
    pool: &Pool<Postgres>
) -> Result<()> {
    /*dbg!(&role);
    let name = role.role.name;
    let title = format!("Role Created: {name}");
    let color = role.role.color;
    let role_id = role.role.id;
    let desc = format!("A new role has been created: {name}.");
    let now = Timestamp::from_secs(SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs() as i64).unwrap();
    let mut e = EmbedBuilder::new()
        .title(title)
        .description(desc)
        .color(color)
        .timestamp(now)
        .footer(EmbedFooterBuilder::new(&format!("Role ID: {role_id}")))
        .validate()?
        .build();

    #[derive(FromRow, Debug)]
    pub struct RoleCreateDbResult { // We have to use this instead of a raw i64
        role_create_notification_channel: Option<Id<ChannelMarker>>,
        default_notification_channel: Option<Id<ChannelMarker>>
    }

    impl RoleCreateDbResult

    let channel = sqlx::query_as::<_, RoleCreateDbResult>("SELECT role_create_notification_channel, default_notification_channel FROM guild_data WHERE guild_id = $1 LIMIT 1")
        .bind(role.guild_id)
        .fetch_optional(&pool.clone())
        .await?
        .unwrap();

    let id: Id<ChannelMarker> = Id::new(1113590994630344776);

    let _ = &client
        .create_message(id)
        .embeds(&[e])
        .unwrap()
        .await?;*/

    Ok(())
}

pub async fn role_delete(
    role: RoleDelete,
    client: &Client
) -> Result<()> {
    dbg!(role);
    Ok(())
}

pub async fn role_update(
    role: RoleUpdate,
    client: &Client
) -> Result<()> {
    dbg!(role);
    Ok(())
}

pub async fn stage_instance_create(
    stage: StageInstanceCreate,
    client: &Client
) -> Result<()> {
    dbg!(stage);
    Ok(())
}

pub async fn stage_instance_delete(
    stage: StageInstanceDelete,
    client: &Client
) -> Result<()> {
    dbg!(stage);
    Ok(())
}

pub async fn stage_instance_update(
    stage: StageInstanceUpdate,
    client: &Client
) -> Result<()> {
    dbg!(stage);
    Ok(())
}

pub async fn thread_create(
    thread: Box<ThreadCreate>,
    client: &Client
) -> Result<()> {
    dbg!(thread);
    Ok(())
}

pub async fn thread_delete(
    thread: ThreadDelete,
    client: &Client
) -> Result<()> {
    dbg!(thread);
    Ok(())
}

pub async fn thread_list_sync(
    thread: ThreadListSync,
    client: &Client
) -> Result<()> {
    dbg!(thread);
    Ok(())
}

pub async fn thread_member_update(
    thread: Box<ThreadMemberUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(thread);
    Ok(())
}

pub async fn thread_members_update(
    thread: ThreadMembersUpdate,
    client: &Client
) -> Result<()> {
    dbg!(thread);
    Ok(())
}

pub async fn thread_update(
    thread: Box<ThreadUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(thread);
    Ok(())
}

pub async fn typing_start(
    typing: Box<TypingStart>,
    client: &Client
) -> Result<()> {
    dbg!(typing);
    Ok(())
}

pub async fn unavailable_guild(
    guild: UnavailableGuild,
    client: &Client
) -> Result<()> {
    dbg!(guild);
    Ok(())
}

pub async fn user_update(
    user: UserUpdate,
    client: &Client
) -> Result<()> {
    dbg!(user);
    Ok(())
}

pub async fn voice_server_update(
    voice: VoiceServerUpdate,
    client: &Client
) -> Result<()> {
    dbg!(voice);
    Ok(())
}

pub async fn voice_state_update(
    state: Box<VoiceStateUpdate>,
    client: &Client
) -> Result<()> {
    dbg!(state);
    Ok(())
}

pub async fn webhooks_update(
    webhooks: WebhooksUpdate,
    client: &Client
) -> Result<()> {
    dbg!(webhooks);
    Ok(())
}
